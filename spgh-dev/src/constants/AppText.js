module.exports = {
    backButton: "Back",
    cancelButton: "Cancel",
    sortButton: "Sort",
    searchPlaceholder: "Search...",
}