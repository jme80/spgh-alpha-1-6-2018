module.exports = {
    seafoamBlue: {
        hex: "#70abad",
        rgb: {
            r: 112,
            g: 171,
            b: 173,
        },
    },
    darkBlue: {
        hex: "#006582",
        rgb: {
            r: 0,
            g: 101,
            b: 130,
        }
    },
    gold: {
        hex: "#eba721",
        rgb: {
            r: 235,
            g: 167,
            b: 33,
        }
    },
    salmon: {
        hex: "#f47e57",
        rgb: {
            r: 244,
            g: 126,
            b: 84,
        }
    },
    plates: {
        starter: {
            hex: "#70abac",
            rgb: {
                r: 112,
                g: 171,
                b: 172,
            }
        },
        bronze: {
            hex: "#825f47",
            rgb: {
                r: 130,
                g: 95,
                b: 71,
            }
        },
        silver: {
            hex: "#9a948e",
            rgb: {
                r: 154,
                g: 148,
                b: 142,
            }
        },
        gold: {
            hex: "#e9ab32",
            rgb: {
                r: 233,
                g: 171,
                b: 50,
            }
        },
        platnium: {
            hex: "#b1b3b6",
            rgb: {
                r: 177,
                g: 179,
                b: 182,
            }
        },
    },
    white: '#fff',
    black: '#333',
    grey: '#ddd',
}
