import React from 'react';

import {
  ActivityIndicator,
  Button,
  ListView,
  RefreshControl,
  StyleSheet,
  Text,
  TextInput,
  View,
  FlatList
} from 'react-native';
import { connect } from 'react-redux';

import _ from 'lodash';

import RestaurantList from '../components/RestaurantList';
import RestaurantsScreenHeader from '../components/RestaurantsScreenHeader';
import Colors from '../styles/Colors'
import SortOptions from '../constants/SortOptions';
import AppText from '../constants/AppText';
import RouteNames from '../constants/RouteNames';

class RestaurantsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
    }
  };

  constructor(props) {
    super(props);

    this.state = {
      restaurants: [],
      isLoading: true,
      searchText: "",
      filteredData: [],
      dataSource: [],
      rawData: [],
      refreshing: false,
      hasError: false,
      appliedSort: SortOptions[0],
      selected: false,
    };

    this.handleSearchTextInput = this.handleSearchTextInput.bind(this);
	this.handleAboutButtonClick = this.handleAboutButtonClick.bind(this);
    this.handleSortButtonClick = this.handleSortButtonClick.bind(this);
  }

  onRefresh() {
    this.setState({
      refreshing: true,
      searchText: "",
    });
    this.fetchData();
  }

  fetchData() {
    var headers = new Headers();
    headers.append('X-SPGH-Token', 'py67C631MZvyGEVEjFURg2dJ4z2yI0XbkRokOcjBe1g');

    var request = new Request('https://spgh-restuarants-api-test.herokuapp.com/api/v1/restaurants', {
      headers: headers
    });
    return fetch(request)
      .then((response) => response.json())
      .then((responseJson) => {

        console.log(responseJson);
        if (responseJson.statusCode && responseJson.statusCode !== 200) {
          this.setState({
            isLoading: false,
            refreshing: false,
            hasError: true,
          });
          return;
        }

        this.setState({
          isLoading: false,
          refreshing: false,
          restaurants: responseJson.data.restaurants,
          filteredData: responseJson.data.restaurants,
          dataSource: responseJson.data.restaurants,
        }, function () {
          // this.sortRestaurantData(this.state.appliedSort);
        });
      })
      .catch((error) => {
        console.error(error);
        this.setState({
          isLoading: false,
          refreshing: false,
          hasError: true,
        });
      });
  }

  textFilterRestaurants(searchText, restaurants) {
    let normalizedSearchText = searchText.toLowerCase();

    return _.filter(restaurants, (restaurant) => {
      let normalizedRestaurantName = restaurant.name.toLowerCase();
      return normalizedRestaurantName.search(normalizedSearchText) !== -1;
    });
  }

  sortRestaurantData(currentSort, restaurants) {
    return _.orderBy(
      restaurants,
      [(item) => {
        if (typeof item[currentSort.key] === "string") {
          return item[currentSort.key].toLowerCase()
        } else {
          return item[currentSort.key]
        }
      }], [currentSort.order]);
  }

  handleAboutButtonClick() {
	  this.props.navigation.navigate(RouteNames.About);
  }
  
  handleSortButtonClick() {
    this.props.navigation.navigate(RouteNames.Sort);
  }

  componentWillMount() {
    console.log('Mounting');
  }
  componentDidMount() {
    this.fetchData();
    console.log('Mounted');
  }

  componentWillUpdate() {
    console.log('Updating');
  }

  componentDidUpdate() {
    if (this.state.appliedSort.id !== this.props.selectedSort.id) {
      let sortedData = this.sortRestaurantData(this.props.selectedSort, this.state.restaurants);
      this.setState({
        dataSource: sortedData,
        appliedSort: this.props.selectedSort,
        selected: !this.state.selected,
      });
    }
    
    this.refs.restaurantListRef.refs.flatListRef.scrollToOffset({x: 0, y: 0, animated: true});
  }

  handleSearchTextInput(searchText) {
    let searchedData = this.textFilterRestaurants(searchText, this.state.restaurants);
    let sortedData = this.sortRestaurantData(this.props.selectedSort, searchedData);
    
    this.setState({
      dataSource: sortedData,
      rawData: this.state.restaurants,
      searchText: searchText,
    });
  }

  render() {
    const { navigate } = this.props.navigation;

    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, flexDirection: 'column', }}>
          <RestaurantsScreenHeader
            setSearchText={this.handleSearchTextInput}
            searchText={this.state.searchText}
            setSort={this.handleSortButtonClick}
			setAbout={this.handleAboutButtonClick}
            sortTitle={AppText.sortButton}
            navigate={this.props.navigate}
          />
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center", }} >
            <ActivityIndicator
              size='large'
              color={Colors.salmon.hex}
            />
          </View>
        </View>
      );
    }

    if (this.state.hasError) {
      return (
        <View style={{ flex: 1, flexDirection: 'column', }}>
          <RestaurantsScreenHeader
            setSearchText={this.handleSearchTextInput}
            searchText={this.state.searchText}
            setSort={this.handleSortButtonClick}
			setAbout={this.handleAboutButtonClick}
            sortTitle={AppText.sortButton}
            navigate={this.props.navigate}
          />
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center", }} >
            <Text>Oops. There was an issue with loading the data. Please press the button to try again.</Text>
            <Button color={Colors.gold.hex} onPress={() => {
                this.setState({isLoading: true, hasError: false, })
                this.fetchData()
              }}
              title={'Reload'}
            />
          </View>
        </View>
      );
    }

    return (
      <View style={{ flex: 1, flexDirection: 'column', }}>
        <RestaurantsScreenHeader
          setSearchText={this.handleSearchTextInput}
          searchText={this.state.searchText}
          setSort={this.handleSortButtonClick}
		  setAbout={this.handleAboutButtonClick}
          sortTitle={AppText.sortButton}
          navigate={this.props.navigate}
        />
          <RestaurantList
            restaurants={this.state.dataSource}
            ref={'restaurantListRef'}
          />
        </View>
    );
  }
}

const mapStateToProps = state => ({
  selectedSort: state.sort,
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default connect(mapStateToProps)(RestaurantsScreen);
