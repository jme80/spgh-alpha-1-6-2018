import React from 'react';

import {
    StyleSheet,
    Text,
    TouchableHighlight,
    View
} from 'react-native';

import _ from 'lodash';

import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import { LatoText } from '../StyledText';
import DesignationPlate from '../DesignationPlate';

import styles from './Styles';
import Colors from '../../styles/Colors';
import RouteNames from '../../constants/RouteNames';

const navigateToRestaurantDetails = (restaurant, restaurantDetails) => {
    restaurantDetails(restaurant);
}

const debounce = _.debounce(navigateToRestaurantDetails, 200);

const RestaurantListItem = ({ restaurant, restaurantDetails }) => (
    <TouchableHighlight
        onPress={() => debounce(restaurant, restaurantDetails)}
        underlayColor={Colors.gold.hex}
    >
        <View style={styles.container}>
            <LatoText numberOfLines={1} ellipsizeMode={'tail'} style={{flex: 1}}>{restaurant.name}</LatoText>
            <DesignationPlate designation={restaurant.designation} style={{ width: 80, height: 80, }}/>
        </View>
    </TouchableHighlight>
)

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
    restaurantDetails: (restaurant) => {
        dispatch(NavigationActions.navigate({ routeName: RouteNames.RestaurantDetails, params: { restaurant }}));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantListItem);
