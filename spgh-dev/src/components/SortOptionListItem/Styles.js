import React from 'react';

import {
    StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({
    listItem: {
        flex: 1,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 10,
        paddingRight: 10,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#ddd',
        height: 60,
    },
});

export default styles;