import LatoText from './LatoText';
import MontserratText from './MontserratText';


module.exports = {
    LatoText,
    MontserratText
};
