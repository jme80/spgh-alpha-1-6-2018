import React from 'react';
import { Text } from 'react-native';

class MontserratText extends React.Component {
  render() {
    return (
      <Text
        {...this.props}
        style={[this.props.style, { fontFamily: 'montserratRegular' }]}
      />
    );
  }
};

export default MontserratText;
