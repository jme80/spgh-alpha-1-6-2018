import React from 'react';
import {
    TouchableOpacity,
} from 'react-native';

import { LatoText } from '../StyledText';
import styles from './Styles';
import BaseHeader from '../BaseHeader';


const RestaurantScreenHeader = ({ navigation }) => (
    <BaseHeader>
        <TouchableOpacity onPress={() => navigation.goBack()}>
            <LatoText style={{ fontSize: 18, paddingLeft: 5, paddingRight: 5, color: '#fff' }}>Back</LatoText>
        </TouchableOpacity>
    </BaseHeader>
)

export default RestaurantScreenHeader;
