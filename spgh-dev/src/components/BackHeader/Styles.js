import { StyleSheet } from 'react-native';
import {
    APPBAR_HEIGHT,
    STATUSBAR_HEIGHT,
} from '../../constants/HeightSettings';

export default styles = StyleSheet.create({
    headerContainer: {
        // flex: 1,
        paddingTop: STATUSBAR_HEIGHT,
        height: STATUSBAR_HEIGHT + APPBAR_HEIGHT,
        paddingLeft: 5,
        paddingRight: 5,
        flexDirection: "row",
        backgroundColor: "#70abad",
        alignItems: "center",
        // position: 'relative',
        justifyContent: "space-between",
    },
    spacerView: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
    },
    titleView: {
        justifyContent: "center"
    },
    headerTitle: {
        fontSize: 16,
        color: "#fff",
    },
    backButton: {
    },
    navigationLogo: {
        width: 50,
        height: 50,
    },
    navigationButton: {
        color: "#fff",
    }
});