import { StyleSheet, Platform } from 'react-native';
import Colors from '../../styles/Colors';

export default styles = StyleSheet.create({
    restaurantDetailsSection: {
        padding: 16,
        flex: 1,
        justifyContent: 'flex-start',
        borderBottomColor: Colors.grey,
        borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection: Platform.OS === 'ios' ? 'row' : 'column',
    },
    restaurantDetailsText: {
        fontSize: 18
    },
    restaurantDetailsLabel: {
        flex: 2,
        paddingBottom: Platform.OS === 'ios' ? 0 : 5,
    },
    restaurantDetailsContent: {
        flex: 1,
    },
})