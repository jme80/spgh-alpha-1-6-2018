import React from 'react';
import {
    View
} from 'react-native';

import {
    PropTypes
} from 'prop-types';

import UrlButton from '../UrlButton';
import { LatoText } from '../StyledText';
import styles from './Styles';

const RestaurantDetailSection = ({ label, content, link }) => {
    if (link) {
        return (
            <View style={styles.restaurantDetailsSection}>
                <LatoText style={styles.restaurantDetailsLabel}>{label}</LatoText>
                <UrlButton
                    uri={link}
                    label={content}
                    style={styles.restaurantDetailsContent}
                />
            </View>
        )
    }

    return (
        <View style={styles.restaurantDetailsSection}>
            <LatoText style={styles.restaurantDetailsLabel}>{label}</LatoText>
            <LatoText style={styles.restaurantDetailsContent}>{content}</LatoText>
        </View>
    )
}

RestaurantDetailSection.propTypes = {
    label: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    link: PropTypes.string,
};

export default RestaurantDetailSection;