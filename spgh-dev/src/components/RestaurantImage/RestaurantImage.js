import React from 'react';
import {
    StyleSheet,
    View
} from 'react-native';

import ImageLoad from 'react-native-image-placeholder';
import {
    PropTypes
} from 'prop-types';

import styles from './Styles';
import RestaurantDetailsHeader from '../RestaurantDetailsHeader';

const RestaurantImage = ({ uri }) => (
    <View>
        <ImageLoad
            style={styles.displayedImage}
            customImagePlaceholderDefaultStyle={styles.loadingStyle}
            placeholderSource={require('../../../assets/icons/spr-logo-250px.png')}
            source={{ uri: uri }}
            isShowActivity={false}
        >
        </ImageLoad>
    </View>
)

RestaurantImage.propTypes = {
    uri: PropTypes.string,
    title: PropTypes.string,
};

export default RestaurantImage;