import React from 'react';
import {
    View
} from 'react-native';
import styles from './Styles';


const BaseHeader = ({style, children}) => (
    <View style={[styles.baseHeader, style, ]}>
        {children}
    </View>
);

export default BaseHeader;
